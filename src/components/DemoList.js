import React, { useMemo } from 'react';
const DemoList= (props) =>{
    const {items} = props;

    const sortedList = props.items.sort((a,b) => a-b);
        
    console.log('Demolist running');

    return(
    <div>
                <h2>{props.title}</h2>
                <ul>
                    {sortedList.map((item) => (
                        <li key={item}>{item}</li>
                    ))}
                </ul>
   
    </div>);
}
export default DemoList;