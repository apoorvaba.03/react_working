//import classes from '*.module.css';
import React from 'react';

const Button = (props) => {
    console.log("inside the button");
    return(
        <button 
            type = {props.type || 'submit'}
          //  className = {`${classes.button} ${classes.props}`}
            onClick={props.onClick}
            disabled={props.disabled}
        
        >
            {props.children}
        </button>
    );
}
export default React.memo(Button);