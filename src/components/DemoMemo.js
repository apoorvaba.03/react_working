import { useCallback, useState } from "react";
import Button from "./Button";
import DemoList from "./DemoList";

function DemoMemo(){
    const [listTitle,setListTitle] = useState('My List');
    const changetitleHandler = useCallback(()=>{
        setListTitle('New Title')},[]
    );

    return(
        <div>
        <DemoList title={listTitle}> items={5,3,1,7,2}</DemoList>
        <Button onClick={changetitleHandler}> clickme</Button>
        </div>
        );
}
export default DemoMemo;