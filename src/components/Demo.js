import React from 'react';
import Myparagraph from './Myparagraph';
//React.memo to avoid unnessary re-evalution of the components when its not changeing the state. 

const Demo = (props) => {
    console.log('inside the demo');
    return(
    
        <Myparagraph>
           
            {props.show ? 'this is new' : ''}
        </Myparagraph>
    );

};
export default React.memo(Demo);