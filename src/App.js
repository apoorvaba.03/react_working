
import './App.css';
import {useCallback, useState,} from 'react';
import Demo from './components/Demo';
import Button from './components/Button';
// callback hook to run the function only ones
// inthis demo shows that working of reactDom and reactjs which all executed on parent components runs.
// React is manging the state and the connection to the components for user
function App() {
const [displaypar,setDisplay] = useState(false);
const [allow,setAllow]= useState(false);

const buttonHandler = useCallback(() => {
  if(allow)
  {
  setDisplay((prevstate)=>(
    !displaypar
  ))
  }},[allow]);

  const allowHandler = () => {
    setAllow(true);
  }

console.log("in appjs");
  return (
    <div>
      <h1>hi welcome</h1>
      {displaypar && <p> this is pargarph</p>}
      <Demo show={displaypar }/>
      <Button onClick = {allowHandler} >Allow me</Button>
      <Button onClick={buttonHandler}>clike me</Button>
    </div>
  );
}

export default App;
